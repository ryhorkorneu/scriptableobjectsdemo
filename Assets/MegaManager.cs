﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MegaManager : MonoBehaviour
{
    [SerializeField] private UnityEvent timeHasCome;
    [SerializeField] private StringUnityEvent userEntered;

    void Start ()
	{
	    userEntered.Invoke("User");
	    Invoke("SetTime", 5f);
	}
	
	void SetTime ()
	{
	    timeHasCome.Invoke();
	}
}
