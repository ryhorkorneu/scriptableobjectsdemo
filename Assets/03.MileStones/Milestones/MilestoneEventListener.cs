﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class UnityEventString : UnityEvent<string> { }

public class MilestoneEventListener : MonoBehaviour
{
    public List<MilestoneEvent> Events;
    public UnityEventMilestone Response;

    private void OnEnable()
    {
        Events.ForEach(e => e.AddListener(this));
    }

    private void OnDisable()
    {
        Events.ForEach(e => e.RemoveListener(this));
    }

    public void OnEventRaised(MilestoneConfig milestone)
    {
        Response.Invoke(milestone);
    }
}

