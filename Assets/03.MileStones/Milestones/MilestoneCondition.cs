﻿using System;
using UnityEngine;
using Iridescent.Core.Utilities;

[Serializable]
[CreateAssetMenu(menuName = "Milestone/MilestoneCondition")]
public class MilestoneCondition : ScriptableObject
{

    private bool value;

    public void SetCondition()
    {
        value = true;
        mainAction.SafeInvoke();
    }

    public bool GetParameter()
    {
        return value;
    }

    public Action mainAction;

    protected internal void AddAction(Action act)
    {
        mainAction += act;
    }

    protected internal void RemoveAction(Action act)
    {
        mainAction -= act;
    }
}
