﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class EventConfig<T> : ScriptableObject
{
    protected readonly List<T> listeners = new List<T>();

    public Action mainAction;

    public abstract void Raise();

    protected internal void AddListener(T listener)
    {
        if (!listeners.Contains(listener)) listeners.Add(listener);
    }

    protected internal void RemoveListener(T listener)
    {
        if (listeners.Contains(listener)) listeners.Remove(listener);
    }

    protected internal void AddAction(Action act)
    {
        mainAction += act;
    }

    protected internal void RemoveAction(Action act)
    {
        mainAction -= act;
    }


}
