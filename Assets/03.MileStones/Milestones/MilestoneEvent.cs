﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Iridescent.Core.Utilities;
using UnityEngine.Events;

[Serializable]
public class UnityEventMilestone : UnityEvent<MilestoneConfig> { }


[CreateAssetMenu(menuName = "Milestone/MilestoneEvent")]
public class MilestoneEvent : EventConfig<MilestoneEventListener>
{
    [SerializeField] private MilestoneConfig milestoneConfig;
    [SerializeField] private List<MilestoneEvent> preConditionalMilestones = new List<MilestoneEvent>();
    [SerializeField] private List<MilestoneCondition> conditions = new List<MilestoneCondition>();

    private bool isUnlocked;
    private int conditionsNumber;

    public void Unlock()
    {
        isUnlocked = true;
    }

    public override void Raise()
    {
        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(milestoneConfig);
        }
        ActionUtility.SafeInvoke(mainAction);
    }

    private void OnEnable()
    {
        //Reset();
        preConditionalMilestones.ForEach(c => c.AddAction(OnConditionTriggered));
        conditions.ForEach(c => c.AddAction(OnConditionTriggered));
    }

    private void OnDisable()
    {
        preConditionalMilestones.ForEach(c => c.RemoveAction(OnConditionTriggered));
        conditions.ForEach(c => c.RemoveAction(OnConditionTriggered));
    }

    public void OnEventRaised(MilestoneConfig milestone)
    {
        OnConditionTriggered();
    }

    public void SetConfig(MilestoneConfig mc)
    {
        milestoneConfig = mc;
    }

    private void OnConditionTriggered()
    {
        Debug.Log(milestoneConfig.milestoneId + " isUnlocked: " + isUnlocked);
        if (isUnlocked) return;
        conditionsNumber++;

        int sumConditions = preConditionalMilestones.Count + conditions.Count;
        if (conditionsNumber >= sumConditions)
        {
            Raise();
            Unlock();
        }

    }

    private void Reset()
    {
        isUnlocked = false;
        conditionsNumber = 0;
    }
}

