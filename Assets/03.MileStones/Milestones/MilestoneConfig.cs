﻿using UnityEngine;

[CreateAssetMenu(menuName = "Milestone/MilestoneConfig")]
public class MilestoneConfig : ScriptableObject
{
    public string milestoneId;

    [Header("Sprites")]
    public Sprite unlockedSprite;

    [Header("Receiving condition")] [Multiline]
    public string condition;
}