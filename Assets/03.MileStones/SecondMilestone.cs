﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SecondMilestone : MonoBehaviour {

    [SerializeField] private UnityEvent mileStoneEvent;

    // Use this for initialization
    void Start ()
    {
        Invoke("RaiseNextCondition", 15f);
    }
	
	void RaiseNextCondition()
	{
	    mileStoneEvent.Invoke();
	}
}
