﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FirstMilestone : MonoBehaviour {

    [SerializeField] private UnityEvent mileStoneEvent;

    // Use this for initialization
    void Start ()
    {
        Invoke("RaiseFirstCondition", 5f);
    }
	
	void RaiseFirstCondition ()
	{
	    mileStoneEvent.Invoke();
	}
}
