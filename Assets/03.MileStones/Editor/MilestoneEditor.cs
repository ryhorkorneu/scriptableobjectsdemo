﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MilestoneConfig))]
public class MilestoneEditor : Editor
{
    MilestoneConfig milestone;

    private GUIStyle textStyle = new GUIStyle();
    private GUIStyle imageStyle = new GUIStyle();

    public void OnEnable()
    {
        milestone = (MilestoneConfig)target;
    }

    public override void OnInspectorGUI()
    {
        textStyle.fontSize = 12;
        textStyle.alignment = TextAnchor.UpperLeft;

        imageStyle.alignment = TextAnchor.UpperCenter;

        DrawDefaultInspector();

        EditorGUILayout.BeginVertical();
        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        EditorGUILayout.LabelField("MilestoneId: ", milestone.milestoneId, textStyle, GUILayout.Height(60));
        

        EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        EditorGUILayout.LabelField("Images: ", "", textStyle, GUILayout.Height(60));

        EditorGUILayout.BeginHorizontal();
        
        if (milestone.unlockedSprite != null)
        {
            GUILayout.Label(milestone.unlockedSprite.texture, imageStyle, GUILayout.Height(100));
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndVertical();

        if (GUI.changed) EditorUtility.SetDirty(milestone);
    }
}