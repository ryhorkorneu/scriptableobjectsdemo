﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MilestoneCondition))]
public class ConditionEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUI.enabled = Application.isPlaying;

        var e = target as MilestoneCondition;
        if (GUILayout.Button("SetCondition"))
            e.SetCondition();
    }
}
