﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MilestoneEvent))]
public class EventEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUI.enabled = Application.isPlaying;

        var e = target as MilestoneEvent;
        if (GUILayout.Button("Raise"))
            e.Raise();
    }
}
