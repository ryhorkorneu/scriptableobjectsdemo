﻿using UnityEngine;

public class SpinWheel : MonoBehaviour
{
    public DemoScriptableObject speedSO;

	void Update ()
	{
	    transform.Rotate(Vector3.forward, Time.deltaTime * speedSO.speed);
	}
}
