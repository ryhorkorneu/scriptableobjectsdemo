﻿using UnityEngine;

[CreateAssetMenu(fileName = "DemoScriptableObject01", menuName = "ScriptableObject/Intro/Demo", order = 1)]
public class DemoScriptableObject : ScriptableObject {

    public string objectName = "First Object";
    public bool isCool = true;
    [Range(0f, 100f)]
    public float speed = 5f;
}
