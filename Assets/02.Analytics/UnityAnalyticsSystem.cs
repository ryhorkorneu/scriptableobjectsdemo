﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;


public class UnityAnalyticsSystem : AnalyticsSystem
{
    public override void SendEvent(string eventName, params AnalyticsParameter[] parameters)
    {
        base.SendEvent(eventName, parameters);
        Debug.Log(eventName + " Sent");
        UnityEngine.Analytics.AnalyticsEvent.Custom(eventName, ConvertToDictionary(parameters));
    }

    public override void SendEvent(string eventName)
    {
        base.SendEvent(eventName);
        Debug.Log(eventName + " Sent");
        UnityEngine.Analytics.AnalyticsEvent.Custom(eventName);
    }


    public override void SendEvent(string eventName, float eventParam)
    {
        base.SendEvent(eventName, eventParam);
        Debug.Log(eventName + " Sent");
        var dic = new Dictionary<string, object>();
        dic.Add(eventName, eventParam);
        UnityEngine.Analytics.AnalyticsEvent.Custom(eventName, dic);
    }

    public override void SendEvent(string eventName, string eventParam)
    {
        base.SendEvent(eventName, eventParam);
        Debug.Log(eventName + " Sent");
        var dic = new Dictionary<string, object>();
        dic.Add(eventName, eventParam);
        UnityEngine.Analytics.AnalyticsEvent.Custom(eventName, dic);
    }

    public override void SendEvent(string eventName, long eventParam)
    {
        base.SendEvent(eventName, eventParam);
        Debug.Log(eventName + " Sent");
        var dic = new Dictionary<string, object>();
        dic.Add(eventName, eventParam);
        UnityEngine.Analytics.AnalyticsEvent.Custom(eventName, dic);
    }

    public override void AdvanceTo(string eventName, string eventParam)
    {
        base.AdvanceTo(eventName, eventParam);
        Debug.Log(eventName + " Sent");
        var dic = new Dictionary<string, object>();
        dic.Add(eventName, eventParam);
        UnityEngine.Analytics.AnalyticsEvent.ScreenVisit(eventParam);
    }

    public override void SetUserId(string eventName, string userId)
    {
        Debug.Log("User Id: " + userId);
        Analytics.SetUserId(userId);
    }
}
