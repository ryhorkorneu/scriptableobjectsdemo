﻿using System;
using System.Linq;

namespace Iridescent.Core.Utilities
{
    /// <summary>
    /// A class for calling safe calling actions
    /// </summary>
    public static class ActionUtility
    {
        public static void SafeInvoke(this Action action)
        {
            if (action != null)
            {
                action.Invoke();
            }
        }

        public static void SafeInvoke<T1>(this Action<T1> action, T1 param1)
        {
            if (action != null)
            {
                action.Invoke(param1);
            }
        }

        public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 param1, T2 param2)
        {
            if (action != null)
            {
                action.Invoke(param1, param2);
            }
        }

        public static void SafeInvoke<T1, T2, T3>(this Action<T1, T2, T3> action, T1 param1, T2 param2, T3 param3)
        {
            if (action != null)
            {
                action.Invoke(param1, param2, param3);
            }
        }

        public static void SafeInvoke<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> action, T1 param1, T2 param2, T3 param3, T4 param4)
        {
            if (action != null)
            {
                action.Invoke(param1, param2, param3, param4);
            }
        }
    }
}