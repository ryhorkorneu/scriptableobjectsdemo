﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Iridescent.Core
{
    
    [Serializable]
    [CreateAssetMenu]
    public class AnalyticsStringParameter : AnalyticsBaseParameter
    {
        public void SetParameter(string s)
        {
            parameterValue = s;
        }

        public override AnalyticsParameter GetParameter()
        {
            return new AnalyticsParameter(parameterName, (string)parameterValue);
        }
    }
}
