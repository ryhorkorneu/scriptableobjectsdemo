﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Iridescent.Core
{
    [Serializable]
    public class AnalyticsParametersEvent : UnityEvent<string, List<AnalyticsParameter>> { }

    public class AnalyticsEventWithParametersListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        public List<AnalyticsEventWithParameters> Events;

        [Tooltip("Response to invoke when Event is raised.")]
        public AnalyticsParametersEvent Response;

        private void OnEnable()
        {
            Events.ForEach(e => e.RegisterListener(this));
        }

        private void OnDisable()
        {
            Events.ForEach(e => e.UnregisterListener(this));
        }

        public void OnEventRaised(string s, List<AnalyticsParameter> parameters)
        {
            Response.Invoke(s, parameters);
        }
    }
}