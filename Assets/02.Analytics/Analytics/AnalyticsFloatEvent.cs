﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Monclarity.HNP
{
    [Serializable]
    [CreateAssetMenu]
    public class AnalyticsFloatEvent : AnalyticsEvent
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<AnalyticsFloatEventListener> eventListeners = 
            new List<AnalyticsFloatEventListener>();

        public void Raise(float eventValue)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(eventName, eventValue);
        }

        public void RegisterListener(AnalyticsFloatEventListener listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }

        public void UnregisterListener(AnalyticsFloatEventListener listener)
        {
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}
