﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Iridescent.Core
{
    
    [Serializable]
    [CreateAssetMenu]
    public class AnalyticsFloatParameter : AnalyticsBaseParameter
    {
        public new float parameterValue;

        public void SetParameter(float f)
        {
            parameterValue = f;
        }

        public override AnalyticsParameter GetParameter()
        {
            return new AnalyticsParameter(parameterName, parameterValue);
        }
    }
}
