﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Monclarity.HNP
{
    [Serializable]
    [CreateAssetMenu]
    public class AnalyticsLongEvent : AnalyticsEvent
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<AnalyticsLongEventListener> eventListeners = 
            new List<AnalyticsLongEventListener>();

        public void Raise(long eventValue)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(eventName, eventValue);
        }

        public void RegisterListener(AnalyticsLongEventListener listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }

        public void UnregisterListener(AnalyticsLongEventListener listener)
        {
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}
