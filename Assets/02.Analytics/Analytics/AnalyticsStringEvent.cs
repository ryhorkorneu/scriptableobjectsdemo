﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Monclarity.HNP
{
    [Serializable]
    [CreateAssetMenu]
    public class AnalyticsStringEvent : AnalyticsEvent
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<AnalyticsStringEventListener> eventListeners = 
            new List<AnalyticsStringEventListener>();

        public void Raise(string eventValue)
        {
            for (int i = eventListeners.Count - 1; i >= 0; i--)
                eventListeners[i].OnEventRaised(eventName, eventValue);
        }

        public void RegisterListener(AnalyticsStringEventListener listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }

        public void UnregisterListener(AnalyticsStringEventListener listener)
        {
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}
