﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Iridescent.Core
{
    
    [Serializable]
    [CreateAssetMenu]
    public class AnalyticsLongParameter : AnalyticsBaseParameter
    {
        public new long parameterValue;

        public void SetParameter(long l)
        {
            parameterValue = l;
        }

        public override AnalyticsParameter GetParameter()
        {
            return new AnalyticsParameter(parameterName, parameterValue);
        }
    }
}
