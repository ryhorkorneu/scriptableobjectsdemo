﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[Serializable]
public class FloatUnityEvent : UnityEvent<float> { }

[Serializable]
public class LongUnityEvent : UnityEvent<long> { }

[Serializable]
[CreateAssetMenu]
public class AnalyticsEvent : ScriptableObject
{
    /// <summary>
    /// The list of listeners that this event will notify if it is raised.
    /// </summary>
    private readonly List<AnalyticsEventListener> eventListeners =
        new List<AnalyticsEventListener>();

    public string eventName;

    public void Raise()
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(eventName);
    }

    public void RegisterListener(AnalyticsEventListener listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(AnalyticsEventListener listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }
}

