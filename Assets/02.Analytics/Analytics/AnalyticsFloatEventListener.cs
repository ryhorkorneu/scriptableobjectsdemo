﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Monclarity.HNP
{
    [Serializable]
    public class AnalyticsFloatParametersEvent : UnityEvent<string, float> { }

    public class AnalyticsFloatEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        public List<AnalyticsFloatEvent> Events;

        [Tooltip("Response to invoke when Event is raised.")]
        public AnalyticsFloatParametersEvent Response;

        private void OnEnable()
        {
            Events.ForEach(e => e.RegisterListener(this));
        }

        private void OnDisable()
        {
            Events.ForEach(e => e.UnregisterListener(this));
        }

        public void OnEventRaised(string eventName, float eventValue)
        {
            Response.Invoke(eventName, eventValue);
        }
    }
}