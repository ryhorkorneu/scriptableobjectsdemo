﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;



[Serializable]
public class StringUnityEvent : UnityEvent<string> { }

public class AnalyticsEventListener : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public List<AnalyticsEvent> Events;

    [Tooltip("Response to invoke when Event is raised.")]
    public StringUnityEvent Response;

    private void OnEnable()
    {
        Events.ForEach(e => e.RegisterListener(this));
    }

    private void OnDisable()
    {
        Events.ForEach(e => e.UnregisterListener(this));
    }

    public void OnEventRaised(string s)
    {
        Response.Invoke(s);
    }
}
