﻿using System.Collections.Generic;
using UnityEngine;


public abstract class AnalyticsSystem : MonoBehaviour
{
    protected bool isInitialized = false;

    public virtual void Init()
    {
        isInitialized = true;
    }

    public virtual void SendEvent(string eventName, params AnalyticsParameter[] parameters)
    {
        if (!isInitialized)
        {
            Debug.LogError(GetType() + " not isInitialized, you can't send " + eventName + " event");
            return;
        }
    }

    public virtual void SendEvent(string eventName, float eventParam)
    {
        if (!isInitialized)
        {
            Debug.LogError(GetType() + " not isInitialized, you can't send " + eventName + " event with params: " + eventParam);
            return;
        }
    }

    public virtual void SendEvent(string eventName, long eventParam)
    {
        if (!isInitialized)
        {
            Debug.LogError(GetType() + " not isInitialized, you can't send " + eventName + " event with params: " + eventParam);
            return;
        }
    }

    public virtual void SendEvent(string eventName, string eventParam)
    {
        if (!isInitialized)
        {
            Debug.LogError(GetType() + " not isInitialized, you can't send " + eventName + " event with params: " + eventParam);
            return;
        }
    }

    public virtual void SendEvent(string eventName)
    {
        if (!isInitialized)
        {
            Debug.LogError(GetType() + " not isInitialized, you can't send " + eventName);
            return;
        }
    }

    public virtual void AdvanceTo(string eventName, string eventParam)
    {
        SendEvent(eventName, eventParam);
    }

    public virtual void AddAttributes(List<AnalyticsParameter> parameters)
    {

    }

    public virtual void SetUserId(string eventName, string userId)
    {
        SendEvent(eventName, userId);
    }



    protected Dictionary<string, object> ConvertToDictionary(params AnalyticsParameter[] parameters)
    {
        var dic = new Dictionary<string, object>();
        foreach (var parameter in parameters)
        {
            switch (parameter.valueType)
            {
                case ParameterType.Float:
                    dic.Add(parameter.GetParameterName(), (float)parameter.parameterDoubleValue);
                    break;
                case ParameterType.Long:
                    dic.Add(parameter.GetParameterName(), (int)parameter.parameterLongValue);
                    break;
                default:
                    dic.Add(parameter.GetParameterName(), parameter.parameterStringValue);
                    break;
            }
        }
        return dic;
    }
}
