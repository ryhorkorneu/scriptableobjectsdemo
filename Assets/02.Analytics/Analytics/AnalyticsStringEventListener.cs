﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Monclarity.HNP
{

    [Serializable]
    public class AnalyticsStringParametersEvent : UnityEvent<string,  string> { }

    public class AnalyticsStringEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        public List<AnalyticsStringEvent> Events;

        [Tooltip("Response to invoke when Event is raised.")]
        public AnalyticsStringParametersEvent Response;

        private void OnEnable()
        {
            Events.ForEach(e => e.RegisterListener(this));
        }

        private void OnDisable()
        {
            Events.ForEach(e => e.UnregisterListener(this));
        }

        public void OnEventRaised(string eventName, string eventValue)
        {
            Response.Invoke(eventName, eventValue);
        }
    }
}