﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Iridescent.Core
{
    [Serializable]
    [CreateAssetMenu]
    public class AnalyticsEventWithParameters : AnalyticsEvent
    {
        /// <summary>
        /// The list of listeners that this event will notify if it is raised.
        /// </summary>
        private readonly List<AnalyticsEventWithParametersListener> eventListeners = 
            new List<AnalyticsEventWithParametersListener>();

        public List<AnalyticsBaseParameter> parameters;
        
       

        public new void Raise()
        {
            for(int i = eventListeners.Count -1; i >= 0; i--)
                eventListeners[i].OnEventRaised(eventName, parameters.Select(p=>p.GetParameter()).ToList());
        }

        public void RegisterListener(AnalyticsEventWithParametersListener listener)
        {
            if (!eventListeners.Contains(listener))
                eventListeners.Add(listener);
        }

        public void UnregisterListener(AnalyticsEventWithParametersListener listener)
        {
            if (eventListeners.Contains(listener))
                eventListeners.Remove(listener);
        }
    }
}
