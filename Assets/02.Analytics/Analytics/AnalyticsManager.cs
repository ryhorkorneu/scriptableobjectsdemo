﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AnalyticsManager : MonoBehaviour
{
    [SerializeField] private List<AnalyticsSystem> analyticsSystems = new List<AnalyticsSystem>();

    void Awake()
    {
        Init();
        Debug.Log("Init");
    }

    private void Init()
    {
        analyticsSystems.ForEach(a => a.Init());
    }

    public void SendSimpleEvent(string eventName)
    {
        analyticsSystems.ForEach(a => a.SendEvent(eventName));
    }

    public void SendEventWithStringValue(string eventName, string eventValue)
    {
        analyticsSystems.ForEach(a => a.SendEvent(eventName, eventValue));
    }

    public void SendEventWithLongValue(string eventName, long eventValue)
    {
        analyticsSystems.ForEach(a => a.SendEvent(eventName, eventValue));
    }

    public void SendEventWithFloatValue(string eventName, float eventValue)
    {
        analyticsSystems.ForEach(a => a.SendEvent(eventName, eventValue));
    }

    public void SendEventWithParameters(string eventName, List<AnalyticsParameter> parameters)
    {
        analyticsSystems.ForEach(a => a.SendEvent(eventName, parameters.ToArray()));
    }

    public void AdvanceTo(string eventName, string eventValue)
    {
        analyticsSystems.ForEach(a => a.AdvanceTo(eventName, eventValue));
    }

    public void AddAttributes(string eventName, List<AnalyticsParameter> parameters)
    {
        analyticsSystems.ForEach(a => a.AddAttributes(parameters));
    }

    public void SetUserId(string eventName, string userId)
    {
        analyticsSystems.ForEach(a => a.SetUserId(eventName, userId));
    }
}

[Serializable]
public class AnalyticsParameter
{
    private string parameterName;
    public ParameterType valueType;
    public string parameterStringValue;
    public long parameterLongValue;
    public double parameterDoubleValue;

    public string GetParameterName()
    {
        return parameterName;
    }

    public AnalyticsParameter(string name, string param)
    {
        parameterName = name;
        valueType = ParameterType.String;
        parameterStringValue = param;
    }

    public AnalyticsParameter(string name, long param)
    {
        parameterName = name;
        valueType = ParameterType.Long;
        parameterLongValue = param;
    }

    public AnalyticsParameter(string name, float param)
    {
        parameterName = name;
        valueType = ParameterType.Float;
        parameterDoubleValue = param;
    }
}

[Serializable]
public enum ParameterType
{
    Float,
    Long,
    String,
}


