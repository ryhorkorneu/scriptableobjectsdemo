﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Iridescent.Core
{
    
    [Serializable]
    public abstract class AnalyticsBaseParameter : ScriptableObject
    {
        public string parameterName;
        public object parameterValue;

        public abstract AnalyticsParameter GetParameter();

        public void SetParameter(object f)
        {
            parameterValue = f;
        }
    }
}
