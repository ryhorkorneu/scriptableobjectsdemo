﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Monclarity.HNP
{

    [Serializable]
    public class AnalyticsLongParametersEvent : UnityEvent<string, long> { }

    public class AnalyticsLongEventListener : MonoBehaviour
    {
        [Tooltip("Event to register with.")]
        public List<AnalyticsLongEvent> Events;

        [Tooltip("Response to invoke when Event is raised.")]
        public AnalyticsLongParametersEvent Response;

        private void OnEnable()
        {
            Events.ForEach(e => e.RegisterListener(this));
        }

        private void OnDisable()
        {
            Events.ForEach(e => e.UnregisterListener(this));
        }

        public void OnEventRaised(string eventName, long eventValue)
        {
            Response.Invoke(eventName, eventValue);
        }
    }
}