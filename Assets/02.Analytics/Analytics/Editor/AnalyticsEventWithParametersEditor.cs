﻿using UnityEditor;
using UnityEngine;

namespace Iridescent.Core
{
    [CustomEditor(typeof(AnalyticsEventWithParameters))]
    public class AnalyticsEventWithParametersEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            AnalyticsEventWithParameters e = target as AnalyticsEventWithParameters;
            if (GUILayout.Button("Raise"))
                e.Raise();
        }
    }
}