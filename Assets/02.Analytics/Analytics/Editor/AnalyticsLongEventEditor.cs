﻿using UnityEditor;
using UnityEngine;

namespace Monclarity.HNP
{
    [CustomEditor(typeof(AnalyticsLongEvent))]
    public class AnalyticsLongEventEditor : Editor
    {
        private long paramValue;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            AnalyticsLongEvent e = target as AnalyticsLongEvent;
            paramValue = EditorGUILayout.LongField("paramValue", paramValue);
            if (GUILayout.Button("Raise"))
                e.Raise(paramValue);
        }
    }
}