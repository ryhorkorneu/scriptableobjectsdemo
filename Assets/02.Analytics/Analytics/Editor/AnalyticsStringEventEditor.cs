﻿using UnityEditor;
using UnityEngine;

namespace Monclarity.HNP
{
    [CustomEditor(typeof(AnalyticsStringEvent))]
    public class AnalyticsStringEventEditor : Editor
    {
        private string paramValue;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            AnalyticsStringEvent e = target as AnalyticsStringEvent;
            paramValue = EditorGUILayout.TextField("paramValue", paramValue);
            if (GUILayout.Button("Raise"))
                e.Raise(paramValue);
        }
    }
}