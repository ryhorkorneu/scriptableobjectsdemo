﻿using UnityEditor;
using UnityEngine;

namespace Monclarity.HNP
{
    [CustomEditor(typeof(AnalyticsFloatEvent))]
    public class AnalyticsFloatEventEditor : Editor
    {
        private float paramValue;
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            AnalyticsFloatEvent e = target as AnalyticsFloatEvent;
            paramValue = EditorGUILayout.FloatField("paramValue", paramValue);
            if (GUILayout.Button("Raise"))
                e.Raise(paramValue);
        }
    }
}