﻿using UnityEditor;
using UnityEngine;

namespace Monclarity.HNP
{
    [CustomEditor(typeof(AnalyticsEvent))]
    public class AnalyticsEventEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            GUI.enabled = Application.isPlaying;

            AnalyticsEvent e = target as AnalyticsEvent;
            if (GUILayout.Button("Raise"))
                e.Raise();
        }
    }
}